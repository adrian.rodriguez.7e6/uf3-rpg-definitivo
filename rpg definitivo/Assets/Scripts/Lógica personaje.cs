using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lógicapersonaje : MonoBehaviour
{
    public float velocidadMovimiento = 5.0f;
    public float velocidadRotacion = 200.0f;
    public Animator anim;
    public float x,y;

    public float velocidadInicial;
    public float velocidadAgachado;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();

        velocidadInicial = velocidadMovimiento;
        velocidadAgachado = velocidadMovimiento * 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        transform.Rotate(0, x * Time.deltaTime * velocidadRotacion, 0);
        transform.Translate(0, 0, y * Time.deltaTime * velocidadMovimiento);

        anim.SetFloat("VelX", x);
        anim.SetFloat("VelY", y);

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            anim.SetBool("Agachado", true);
            velocidadMovimiento = velocidadInicial;
        }
        else
        {
            anim.SetBool("Agachado", false);
            velocidadMovimiento = velocidadAgachado;
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            anim.SetBool("Aim", true);
        }
        else
        {
            anim.SetBool("Aim", false);
        }
    }
}
