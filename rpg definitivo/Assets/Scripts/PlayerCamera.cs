using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerCamera : MonoBehaviour
{
    [SerializeField] CinemachineVirtualCamera thirdPersonCam;
    [SerializeField] CinemachineVirtualCamera firstPersonCam;

    private void OnEnable()
    {
        SwitchCamera.Register(firstPersonCam);
        SwitchCamera.Register(thirdPersonCam);
        SwitchCamera.SwitchCameraFunction(thirdPersonCam);
    }

    private void OnDisable()
    {
        SwitchCamera.Unregister(firstPersonCam);
        SwitchCamera.Unregister(thirdPersonCam);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (SwitchCamera.IsActiveCamera(thirdPersonCam))
            {
                SwitchCamera.SwitchCameraFunction(firstPersonCam);
            }
            else if (SwitchCamera.IsActiveCamera(firstPersonCam))
            {
                SwitchCamera.SwitchCameraFunction(thirdPersonCam);
            }
        }
    }
}
