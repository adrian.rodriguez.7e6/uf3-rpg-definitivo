using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class input : MonoBehaviour
{
    PlayerInput playerInput;
    InputAction moveAction;

    [SerializeField] float speed = 5;

    public void Start()
    {
        playerInput = GetComponent<PlayerInput>();
        moveAction = playerInput.FindAction("Movement");
    }

    public void Update()
    {
        MovePlayer();
    }

    public void MovePlayer()
    {
        Vector2 direction = moveAction.ReadValue<Vector2>();
        transform.position += new Vector3(direction.x, 0, direction.y) * speed * Time.deltaTime;
    }
}
